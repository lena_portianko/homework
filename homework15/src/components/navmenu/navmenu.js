import React from 'react';
import './navmenu.css';

const NavMenu=()=>{
    return (
        <ul>
            <li>home</li>
            <li>photoapp</li>
            <li>design</li>
            <li>download</li>
        </ul>
    )
}

export default NavMenu